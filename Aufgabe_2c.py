from __future__ import annotations
from dataclasses import dataclass


class Stack(list):
    def push(self, x):
        self.append(x)

    def isEmpty(self):
        return len(self) == 0

@dataclass
class Knoten():
    k: int
    v: object = None
    pred: Knoten = None
    succ: Knoten = None
    down: Knoten = None

class SkipListe():
    listen: list[Knoten] = []

    def __init__(self):
        """Erzeugt eine leere SkipListe."""

    def min(self):
        """Gibt den Knoten mit dem kleinsten Schlüssel zurück."""

    def max(self):
        """Gibt den Knoten mit dem größten Schlüssel zurück."""

    def get(self, k):
        """Gibt den Knoten mit dem Schlüssel k zurück."""

    def pred(self, k):
        """Gibt den Vorgängerknoten des Knotens mit dem Schlüssel k zurück."""

    def succ(self, k):
        """Gibt den Nachfolgerknoten des Knotens mit dem Schlüssel k zurück."""

    def search(self, k):
        """Gibt einen Stack, welcher alle Vorgängerknoten aller Knoten mit dem
        Schlüssel k in allen Listen der SkipListe (von unten nach oben)
        enthält, zurück.
        """

    def put(self, k, v):
        """Fügt einen Knoten mit dem Wert v und Schlüssel k an der richtigen
        Stelle ein.
        """

    def remove(self, k):
        """Entfernt alle Knoten mit dem Schlüssel k aus allen Listen."""
